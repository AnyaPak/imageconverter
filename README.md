# Simple image converting app which resizes original img to specified width and height and converts it to grayscale

###Comments:
- Checkout: `git clone https://gitlab.com/Pak/imageconverter.git cd imageconverter`
- Build: `mvn package`
- Run: `cd target && java -jar imageConverter.jar url width height`
