package com.company;

import java.awt.*;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;

public class ImageProcessingUtils {

    static BufferedImage resize(final BufferedImage image, final int width) {
        return resize(image, width, width);
    }

    static BufferedImage resize(final BufferedImage image, final int width, final int height) {
        final BufferedImage newImage = new BufferedImage(width, height, image.getType());
        final Graphics2D g = newImage.createGraphics();
        g.drawImage(image, 0, 0, width, height, null);
        g.dispose();
        return newImage;
    }

    static BufferedImage toGrayScale(final BufferedImage image) {
        BufferedImage gray = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());
        ColorConvertOp op = new ColorConvertOp(ColorSpace.getInstance(ColorSpace.CS_GRAY), null);
        op.filter(image, gray);
        return gray;
    }

}
