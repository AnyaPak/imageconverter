package com.company;

import org.apache.commons.io.FilenameUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    private static final Logger LOGGER = Logger.getLogger(Main.class.getName());

    private static final String WIDTH = "width";
    private static final String HEIGHT = "height";

    public static void main(String[] args) {

        if (args.length < 3) {
            throw new IllegalArgumentException("Not enough arguments (url, width, height expected)");
        }

        final int width = parsePosInt(args[1], WIDTH);
        final int height = parsePosInt(args[2], HEIGHT);

        final BufferedImage image;
        final URL url;

        try {
            url = new URL(args[0]);
            image = ImageIO.read(url);
        } catch (final IOException e) {
            throw new RuntimeException(String.format("Unable to read image from <%s>", args[0]), e);
        }

        final BufferedImage resized = ImageProcessingUtils.resize(image, width, height);
        final BufferedImage grayScale = ImageProcessingUtils.toGrayScale(resized);

        try {
            final String extension = "".equals(FilenameUtils.getExtension(url.getPath())) ? "jpg" : FilenameUtils.getExtension(url.getPath());
            final File f = new File(FilenameUtils.getBaseName(url.getPath())+"_grayscale."+extension);
            LOGGER.log(Level.INFO, "Converted image will be saved to {0}", f.getAbsolutePath());
            ImageIO.write(grayScale, extension, f);
        } catch(IOException e) {
            throw new RuntimeException("Unable to save image", e);
        }

    }

    private static int parsePosInt(final String value, final String valueName) {
        try {
            final int parsed = Integer.parseInt(value);
            if (parsed > 1) {
                return parsed;
            }
            throw new IllegalArgumentException(String.format("incorrect value of <%s>", valueName));
        } catch (final NumberFormatException e) {
            throw new IllegalArgumentException(String.format("Unable to parse <%s> value as %s", value, valueName));
        }
    }

}
