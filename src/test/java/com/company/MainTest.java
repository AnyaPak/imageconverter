package com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class MainTest {

    @Test(expected = IllegalArgumentException.class)
    public void mainNotEnoughArguments() {
        Main.main(new String[]{"100", "file"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void incorrectWidth() {
        Main.main(new String[]{"file", "aaa", "100"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void incorrectHeight() {
        Main.main(new String[]{"file", "100", "aaa"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void negativeValue() {
        Main.main(new String[]{"file", "-100", "100"});
    }
}